<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM perpustakaan WHERE id = :id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$perpustakaan = $statement->fetch(PDO::FETCH_OBJ);
if (isNotEmptyEdit())
{
    $nis = $_POST['nis'];
    $nama = $_POST['nama']; 
    $kelas = $_POST['kelas'];
    $buku = $_POST['buku'];
    $batas = $_POST['batas'];
    $sql = 'UPDATE perpustakaan SET nis=:nis, nama=:nama, kelas=:kelas, buku=:buku, batas=:batas WHERE id=:id';
    $statement = $connection->prepare($sql);
    if ($statement->execute([':nis' => $nis, ':nama' => $nama, ':kelas' => $kelas, ':buku' => $buku, ':batas' => $batas, ':id' => $id ]))
    {
        header("Location:data_siswa.php");
    }
    
    
}

function isNotEmptyEdit()
{
    
    return isset ($_POST['nis']) && isset ($_POST['nama']) && isset ($_POST['kelas']) && isset ($_POST['buku']) && isset ($_POST['batas']);
    
}

?>

<?php require 'header.php'; ?>
<div class="container mt-5">
    <div class="row">
        <div class="col">
            <div class="card mt-5">
                <div class="card-header">
                    <h2>Edit Data Siswa</h2>
                </div>
                <div class="card-body">
                    <?php if(!empty($message)): ?>
                    <div class="alert alert-success">
                        <?php echo $message; ?>
                    </div>
                    <?php endif; ?>    
                    <form method="post">
                        <div class="form-group">
                            <label>Nis</label>
                            <input maxlength="20" type="text" value="<?php echo $perpustakaan->nis; ?>" name="nis" id="nis" class="form-control" placeholder="Input Nis Anda Disini" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Nama</label>
                            <input maxlength="20"  type="text" value="<?php echo $perpustakaan->nama; ?>" name="nama" id="nama" class="form-control" placeholder="Input Nama Anda Disini" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Kelas</label>
                            <input maxlength="20"  type="text" value="<?php echo $perpustakaan->kelas; ?>" name="kelas" id="kelas" class="form-control" placeholder="Input Kelas Anda Disini" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Judul Buku</label>
                            <input maxlength="20"  type="text" value="<?php echo $perpustakaan->buku; ?>" name="buku" id="buku" class="form-control" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label>Batas Waktu Peminjaman Buku</label>
                            <select value="<?php echo $perpustakaan->batas; ?>" class="form-control" name="batas" id="batas" required>
                                <option></option>
                                <option value="3 Hari">3 Hari</option>
                                <option value="6 Hari">6 Hari</option>
                                <option value="9 Hari">9 Hari</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>    
    </div> 
</div>
<?php require 'footer.php'; ?>