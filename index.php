<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="shortcut icon" href="images/logo.png">
    <title>Perpustaakan Sekolah</title>
    
    <style>
        body{
            background:url(images/download.jpg)no-repeat fixed;
            background-size: cover;
        }
        #col{
            font-size: 20px;
            background-color: #f0f8ff;
        }
    </style>
</head>
<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-secondary">
        <div class="container-5">
            <a class="navbar-brand text-light"><img src="images/buku.png" width="45" alt=""><b> Perpustakaan .Id</b></a>
        </div>
    </nav>
    
    <div class=" text-center mt-5 p-5">
        <div class="pull-right mt-5 text-light"> 
            <img src="images/zzzz.png" width="145" alt="">
            <h2></h2>
            <h4><b>Untuk Meminjam Buku Di Perpustakaan Kami</b></h4>
            <h4>Silahkan Klik Daftar </h4>
        </div>
        <a href="create.php" class="btn btn-primary">Daftar</a>
    </div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>